package no.accelerate.character.test3;

import no.accelerate.heros.Rogue;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RogueDefaultAttributesTest {

    @Test
    public void rogue_defaultAttributeName_shouldBeRogue() {
        // Arrange
        String expected = "Rogue";
        Rogue rogue1 = new Rogue();
        // Act
        String actual = rogue1.getName();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void rogue_defaultAttributeStrength_shouldBe2() {
        // Arrange
        int expected = 2;
        Rogue rogue1 = new Rogue();
        // Act
        int actual = rogue1.getStrength();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void rogue_defaultAttributeDexterity_shouldBe6() {
        // Arrange
        int expected = 6;
        Rogue rogue1 = new Rogue();
        // Act
        int actual = rogue1.getDexterity();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void rogue_defaultAttributeIntelligence_shouldBe1() {
        // Arrange
        int expected = 1;
        Rogue rogue1 = new Rogue();
        // Act
        int actual = rogue1.getIntelligence();
        // Assert
        assertEquals(expected,actual);
    }

}
