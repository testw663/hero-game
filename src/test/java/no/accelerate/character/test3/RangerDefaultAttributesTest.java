package no.accelerate.character.test3;

import no.accelerate.heros.Ranger;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RangerDefaultAttributesTest {

    @Test
    public void ranger_defaultAttributeName_shouldBeRanger() {
        // Arrange
        String expected = "Ranger";
        Ranger ranger1 = new Ranger();
        // Act
        String actual = ranger1.getName();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void ranger_defaultAttributeStrength_shouldBe1() {
        // Arrange
        int expected = 1;
        Ranger ranger1 = new Ranger();
        // Act
        int actual = ranger1.getStrength();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void ranger_defaultAttributeDexterity_shouldBe7() {
        // Arrange
        int expected = 7;
        Ranger ranger1 = new Ranger();
        // Act
        int actual = ranger1.getDexterity();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void ranger_defaultAttributeIntelligence_shouldBe1() {
        // Arrange
        int expected = 1;
        Ranger ranger1 = new Ranger();
        // Act
        int actual = ranger1.getIntelligence();
        // Assert
        assertEquals(expected,actual);
    }

}
