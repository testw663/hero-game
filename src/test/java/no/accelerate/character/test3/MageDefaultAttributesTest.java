package no.accelerate.character.test3;

import no.accelerate.heros.Mage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MageDefaultAttributesTest {

    @Test
    public void mage_defaultAttributeName_shouldBeMage() {
        // Arrange
        String expected = "Mage";
        Mage mage1 = new Mage();
        // Act
        String actual = mage1.getName();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void mage_defaultAttributeStrength_shouldBe1() {
        // Arrange
        int expected = 1;
        Mage mage1 = new Mage();
        // Act
        int actual = mage1.getStrength();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void mage_defaultAttributeDexterity_shouldBe1() {
        // Arrange
        int expected = 1;
        Mage mage1 = new Mage();
        // Act
        int actual = mage1.getDexterity();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void mage_defaultAttributeIntelligence_shouldBe8() {
        // Arrange
        int expected = 8;
        Mage mage1 = new Mage();
        // Act
        int actual = mage1.getIntelligence();
        // Assert
        assertEquals(expected,actual);
    }

}
