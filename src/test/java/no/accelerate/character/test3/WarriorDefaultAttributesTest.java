package no.accelerate.character.test3;

import no.accelerate.heros.Warrior;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WarriorDefaultAttributesTest {

    @Test
    public void warrior_defaultAttributeName_shouldBeWarrior() {
        // Arrange
        String expected = "Warrior";
        Warrior warrior1 = new Warrior();
        // Act
        String actual = warrior1.getName();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void warrior_defaultAttributeStrength_shouldBe5() {
        // Arrange
        int expected = 5;
        Warrior warrior1 = new Warrior();
        // Act
        int actual = warrior1.getStrength();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void warrior_defaultAttributeDexterity_shouldBe2() {
        // Arrange
        int expected = 2;
        Warrior warrior1 = new Warrior();
        // Act
        int actual = warrior1.getDexterity();
        // Assert
        assertEquals(expected,actual);
    }

    @Test
    public void warrior_defaultAttributeIntelligence_shouldBe1() {
        // Arrange
        int expected = 1;
        Warrior warrior1 = new Warrior();
        // Act
        int actual = warrior1.getIntelligence();
        // Assert
        assertEquals(expected,actual);
    }

}
