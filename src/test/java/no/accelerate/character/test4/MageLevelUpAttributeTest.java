package no.accelerate.character.test4;

import no.accelerate.heros.Mage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MageLevelUpAttributeTest {

    @Test
    public void mage_levelUpAttributeLevel_shouldBe2() {
        // Arrange
        int expected = 2;
        Mage mage1 = new Mage();
        mage1.levelUp();
        // Act
        int actual = mage1.getLevel();
        // Assert
        assertEquals(expected,actual);
    }

    public void mage_levelUpAttributeStrength_shouldBe2() {
        // Arrange
        int expected = 2;
        Mage mage1 = new Mage();
        mage1.levelUp();
        // Act
        int actual = mage1.getStrength();
        // Assert
        assertEquals(expected,actual);
    }

    public void mage_levelUpAttributeDexterity_shouldBe2() {
        // Arrange
        int expected = 2;
        Mage mage1 = new Mage();
        mage1.levelUp();
        // Act
        int actual = mage1.getDexterity();
        // Assert
        assertEquals(expected,actual);
    }

    public void mage_levelUpAttributeIntelligence_shouldBe13() {
        // Arrange
        int expected = 13;
        Mage mage1 = new Mage();
        mage1.levelUp();
        // Act
        int actual = mage1.getIntelligence();
        // Assert
        assertEquals(expected,actual);
    }
}
