package no.accelerate.character.test4;

import no.accelerate.heros.Ranger;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RangerLevelUpAttributeTest {

    @Test
    public void ranger_levelUpAttributeLevel_shouldBe2() {
        // Arrange
        int expected = 2;
        Ranger ranger1 = new Ranger();
        ranger1.levelUp();
        // Act
        int actual = ranger1.getLevel();
        // Assert
        assertEquals(expected,actual);
    }

    public void ranger_levelUpAttributeStrength_shouldBe2() {
        // Arrange
        int expected = 2;
        Ranger ranger1 = new Ranger();
        ranger1.levelUp();
        // Act
        int actual = ranger1.getStrength();
        // Assert
        assertEquals(expected,actual);
    }

    public void ranger_levelUpAttributeDexterity_shouldBe12() {
        // Arrange
        int expected = 12;
        Ranger ranger1 = new Ranger();
        ranger1.levelUp();
        // Act
        int actual = ranger1.getDexterity();
        // Assert
        assertEquals(expected,actual);
    }

    public void ranger_levelUpAttributeIntelligence_shouldBe2() {
        // Arrange
        int expected = 2;
        Ranger ranger1 = new Ranger();
        ranger1.levelUp();
        // Act
        int actual = ranger1.getIntelligence();
        // Assert
        assertEquals(expected,actual);
    }
}
