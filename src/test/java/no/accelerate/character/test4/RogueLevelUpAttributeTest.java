package no.accelerate.character.test4;

import no.accelerate.heros.Rogue;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RogueLevelUpAttributeTest {

    @Test
    public void rogue_levelUpAttributeLevel_shouldBe2() {
        // Arrange
        int expected = 2;
        Rogue rogue1 = new Rogue();
        rogue1.levelUp();
        // Act
        int actual = rogue1.getLevel();
        // Assert
        assertEquals(expected,actual);
    }

    public void rogue_levelUpAttributeStrength_shouldBe3() {
        // Arrange
        int expected = 3;
        Rogue rogue1 = new Rogue();
        rogue1.levelUp();
        // Act
        int actual = rogue1.getStrength();
        // Assert
        assertEquals(expected,actual);
    }

    public void rogue_levelUpAttributeDexterity_shouldBe10() {
        // Arrange
        int expected = 10;
        Rogue rogue1 = new Rogue();
        rogue1.levelUp();
        // Act
        int actual = rogue1.getDexterity();
        // Assert
        assertEquals(expected,actual);
    }

    public void rogue_levelUpAttributeIntelligence_shouldBe2() {
        // Arrange
        int expected = 2;
        Rogue rogue1 = new Rogue();
        rogue1.levelUp();
        // Act
        int actual = rogue1.getIntelligence();
        // Assert
        assertEquals(expected,actual);
    }
}
