package no.accelerate.character.test4;

import no.accelerate.heros.Warrior;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WarriorLevelUpAttributeTest {

    @Test
    public void warrior_levelUpAttributeLevel_shouldBe2() {
        // Arrange
        int expected = 2;
        Warrior warrior1 = new Warrior();
        warrior1.levelUp();
        // Act
        int actual = warrior1.getLevel();
        // Assert
        assertEquals(expected,actual);
    }

    public void warrior_levelUpAttributeStrength_shouldBe8() {
        // Arrange
        int expected = 8;
        Warrior warrior1 = new Warrior();
        warrior1.levelUp();
        // Act
        int actual = warrior1.getStrength();
        // Assert
        assertEquals(expected,actual);
    }

    public void warrior_levelUpAttributeDexterity_shouldBe4() {
        // Arrange
        int expected = 4;
        Warrior warrior1 = new Warrior();
        warrior1.levelUp();
        // Act
        int actual = warrior1.getDexterity();
        // Assert
        assertEquals(expected,actual);
    }

    public void warrior_levelUpAttributeIntelligence_shouldBe2() {
        // Arrange
        int expected = 2;
        Warrior warrior1 = new Warrior();
        warrior1.levelUp();
        // Act
        int actual = warrior1.getIntelligence();
        // Assert
        assertEquals(expected,actual);
    }
}
