package no.accelerate.character.test2;

import no.accelerate.heros.Mage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MageLevelUpTest {

    @Test
    public void mage_levelUp_shouldBe2() {
        // Arrange
        int expected = 2;
        Mage mage1 = new Mage();
        mage1.levelUp();
        // Act
        int actual = mage1.getLevel();
        // Assert
        assertEquals(expected,actual);
    }
}




