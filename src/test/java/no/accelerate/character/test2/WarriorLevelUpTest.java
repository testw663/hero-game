package no.accelerate.character.test2;

import no.accelerate.heros.Warrior;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WarriorLevelUpTest {

    @Test
    public void warrior_levelUp_shouldBe2() {
        // Arrange
        int expected = 2;
        Warrior warrior1 = new Warrior();
        warrior1.levelUp();
        // Act
        int actual = warrior1.getLevel();
        // Assert
        assertEquals(expected,actual);
    }
}




