package no.accelerate.character.test2;

import no.accelerate.heros.Ranger;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RangerLevelUpTest {

    @Test
    public void ranger_levelUp_shouldBe2() {
        // Arrange
        int expected = 2;
        Ranger ranger1 = new Ranger();
        ranger1.levelUp();
        // Act
        int actual = ranger1.getLevel();
        // Assert
        assertEquals(expected,actual);
    }
}




