package no.accelerate.character.test2;

import no.accelerate.heros.Rogue;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RogueLevelUpTest {

    @Test
    public void rogue_levelUp_shouldBe2() {
        // Arrange
        int expected = 2;
        Rogue rogue1 = new Rogue();
        rogue1.levelUp();
        // Act
        int actual = rogue1.getLevel();
        // Assert
        assertEquals(expected,actual);
    }
}




