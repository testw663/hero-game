package no.accelerate.character.test1;

import no.accelerate.heros.Mage;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class MageLevel1Test {
    @Test
    public void mageCreation_level_shouldBe1() {
        // Arrange
        int expected = 1;
        Mage mage1 = new Mage();
        // Act
        int actual = mage1.getLevel();
        // Assert
        assertEquals(expected,actual);
    }
}
