package no.accelerate.character.test1;

import no.accelerate.heros.Ranger;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RangerLevel1Test {
    @Test
    public void rangerCreation_level_shouldBe1() {
        // Arrange
        int expected = 1;
        Ranger ranger1 = new Ranger();
        // Act
        int actual = ranger1.getLevel();
        // Assert
        assertEquals(expected,actual);
    }
}
