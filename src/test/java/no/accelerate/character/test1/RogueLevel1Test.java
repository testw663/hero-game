package no.accelerate.character.test1;

import no.accelerate.heros.Rogue;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class RogueLevel1Test {
    @Test
    public void rogueCreation_level_shouldBe1() {
        // Arrange
        int expected = 1;
        Rogue rogue1 = new Rogue();
        // Act
        int actual = rogue1.getLevel();
        // Assert
        assertEquals(expected,actual);
    }
}

