package no.accelerate.character.test1;

import no.accelerate.heros.Warrior;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class WarriorLevel1Test {
    @Test
    public void warriorCreation_level_shouldBe1() {
        // Arrange
        int expected = 1;
        Warrior warrior1 = new Warrior();
        // Act
        int actual = warrior1.getLevel();
        // Assert
        assertEquals(expected,actual);
    }
}

