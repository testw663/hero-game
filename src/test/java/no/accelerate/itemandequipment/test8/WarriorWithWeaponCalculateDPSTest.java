package no.accelerate.itemandequipment.test8;

import no.accelerate.heros.Warrior;
import no.accelerate.items.Slot;
import no.accelerate.items.Weapon;
import no.accelerate.items.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WarriorWithWeaponCalculateDPSTest {

    @Test
    public void warriorDPS_withWeapon_shouldReturnCorrectValue() {
        // Arrange
        double expected = ((7*1.1)*(1+(5/100.0)));

        Warrior warrior1 = new Warrior();

        Weapon axe = new Weapon();
        axe.setWeaponType(WeaponType.Axe);
        axe.setName("Axe");
        axe.setLevel(1);
        axe.setDamage(7);
        axe.setAttackPerSecond(1.1);
        axe.setSlot(Slot.WEAPON);

        warrior1.setEquipment(axe);

        // Act & Assert
        double actual = warrior1.getDPS();
        assertEquals(expected, actual);

    }
}

