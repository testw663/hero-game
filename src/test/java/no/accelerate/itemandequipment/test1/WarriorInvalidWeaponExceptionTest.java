package no.accelerate.itemandequipment.test1;

import no.accelerate.exceptions.InvalidWeaponException;
import no.accelerate.heros.Warrior;
import no.accelerate.items.Slot;
import no.accelerate.items.Weapon;
import no.accelerate.items.WeaponType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class WarriorInvalidWeaponExceptionTest {

    private no.accelerate.exceptions.InvalidWeaponException InvalidWeaponException;

    @Test
    public void warriorSetEquipment_weaponLevelTooHigh_shouldThrowInvalidWeaponException() {
        // Arrange
        String expected = "The required level for this weapon is too high.";

        Warrior warrior1 = new Warrior();

        Weapon axe = new Weapon();
        axe.setWeaponType(WeaponType.Axe);
        axe.setName("Axe");
        axe.setLevel(2);
        axe.setDamage(10);
        axe.setAttackPerSecond(0.2);
        axe.setSlot(Slot.WEAPON);

        // Act & Assert
        Exception exception = assertThrows(InvalidWeaponException.class,
                () -> warrior1.setEquipment(axe));
        String actual = exception.getMessage();
        assertEquals(expected, actual);

    }
}


