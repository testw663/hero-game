package no.accelerate.itemandequipment.test9;

import no.accelerate.PrimaryAttribute;
import no.accelerate.heros.Warrior;
import no.accelerate.items.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WarriorWithWeaponAndArmorCalculateDPSTest {

    @Test
    public void warriorDPS_withWeaponAndArmor_shouldReturnCorrectValue() {
        // Arrange
        double expected = ((7*1.1)*(1+((5+1)/100.0)));

        Warrior warrior1 = new Warrior();

        Weapon axe = new Weapon();
        axe.setWeaponType(WeaponType.Axe);
        axe.setName("Axe");
        axe.setLevel(1);
        axe.setDamage(7);
        axe.setAttackPerSecond(1.1);
        axe.setSlot(Slot.WEAPON);

        warrior1.setEquipment(axe);

        Armor mailBody = new Armor();
        mailBody.setName("Plate");
        mailBody.setLevel(1);
        mailBody.setSlot(Slot.BODY);
        mailBody.setArmorType(ArmorType.PLATE);
        mailBody.setAttrArmor(new PrimaryAttribute(1, 0 ,0));

        warrior1.setEquipment(mailBody);

        // Act & Assert
        double actual = warrior1.getDPS();
        assertEquals(expected, actual);

    }
}

