package no.accelerate.itemandequipment.test4;

import no.accelerate.PrimaryAttribute;
import no.accelerate.heros.Warrior;
import no.accelerate.items.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class WarriorInvalidArmorExceptionTest {

    private no.accelerate.exceptions.InvalidArmorException InvalidArmorException;

    @Test
    public void warriorSetArmor_wrongArmorType_shouldThrowInvalidArmorException() {
        // Arrange
        String expected = "Warrior can only use armor: Mail and Plate";

        Warrior warrior1 = new Warrior();

        Armor clothLeg = new Armor();
        clothLeg.setName("Cloth");
        clothLeg.setLevel(1);
        clothLeg.setSlot(Slot.LEGS);
        clothLeg.setArmorType(ArmorType.CLOTH);
        clothLeg.setAttrArmor(new PrimaryAttribute(0, 4 ,0));

        // Act & Assert
        Exception exception = assertThrows(no.accelerate.exceptions.InvalidArmorException.class,
                () -> warrior1.setEquipment(clothLeg));
        String actual = exception.getMessage();
        assertEquals(expected, actual);

    }
}


