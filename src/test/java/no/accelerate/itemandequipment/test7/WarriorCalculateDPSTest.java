package no.accelerate.itemandequipment.test7;

import no.accelerate.PrimaryAttribute;
import no.accelerate.heros.Warrior;
import no.accelerate.items.Armor;
import no.accelerate.items.ArmorType;
import no.accelerate.items.Slot;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WarriorCalculateDPSTest {

    @Test
    public void warriorDPS_noWeapon_shouldReturnCorrectValue() {
        // Arrange
        double expected = (1*(1+(5/100.0)));

        Warrior warrior1 = new Warrior();


        // Act & Assert
        double actual = warrior1.getDPS();
        assertEquals(expected, actual);

    }
}

