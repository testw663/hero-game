package no.accelerate.itemandequipment.test5;

import no.accelerate.PrimaryAttribute;
import no.accelerate.heros.Warrior;
import no.accelerate.items.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class WarriorValidWeaponTest {

    private no.accelerate.exceptions.InvalidArmorException InvalidArmorException;

    @Test
    public void warriorSetWeapon_validWeaponType_shouldReturnTrue() {
        // Arrange
        boolean expected = true;

        Warrior warrior1 = new Warrior();

        Weapon axe = new Weapon();
        axe.setWeaponType(WeaponType.Axe);
        axe.setName("Axe");
        axe.setLevel(1);
        axe.setDamage(20);
        axe.setAttackPerSecond(0.1);
        axe.setSlot(Slot.WEAPON);

        // Act & Assert
        boolean actual = warrior1.setEquipment(axe);
        assertEquals(expected, actual);
    }
}


