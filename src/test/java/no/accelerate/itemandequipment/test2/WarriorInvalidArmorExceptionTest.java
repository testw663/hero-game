package no.accelerate.itemandequipment.test2;

import no.accelerate.PrimaryAttribute;
import no.accelerate.exceptions.InvalidArmorException;
import no.accelerate.exceptions.InvalidWeaponException;
import no.accelerate.heros.Warrior;
import no.accelerate.items.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class WarriorInvalidArmorExceptionTest {

    private no.accelerate.exceptions.InvalidWeaponException InvalidWeaponException;

    @Test
    public void warriorSetArmor_armorLevelTooHigh_shouldThrowInvalidArmorException() {
        // Arrange
        String expected = "The required level for this armor is too high.";

        Warrior warrior1 = new Warrior();

        Armor plateBody = new Armor();
        plateBody.setName("Plate");
        plateBody.setLevel(2);
        plateBody.setSlot(Slot.BODY);
        plateBody.setArmorType(ArmorType.PLATE);
        plateBody.setAttrArmor(new PrimaryAttribute(1, 0 ,0));

        // Act & Assert
        Exception exception = assertThrows(InvalidArmorException.class,
                () -> warrior1.setEquipment(plateBody));
        String actual = exception.getMessage();
        assertEquals(expected, actual);

    }
}


