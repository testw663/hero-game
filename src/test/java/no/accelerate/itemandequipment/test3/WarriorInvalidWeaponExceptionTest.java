package no.accelerate.itemandequipment.test3;

import no.accelerate.PrimaryAttribute;
import no.accelerate.exceptions.InvalidArmorException;
import no.accelerate.exceptions.InvalidWeaponException;
import no.accelerate.heros.Warrior;
import no.accelerate.items.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class WarriorInvalidWeaponExceptionTest {

    private no.accelerate.exceptions.InvalidWeaponException InvalidWeaponException;

    @Test
    public void warriorSetWeapon_wrongWeaponType_shouldThrowInvalidWeaponException() {
        // Arrange
        String expected = "Warrior can only use weapons: Axe, Hammer and Sword";

        Warrior warrior1 = new Warrior();

        Weapon bow = new Weapon();
        bow.setWeaponType(WeaponType.Bow);
        bow.setName("Bow");
        bow.setLevel(1);
        bow.setDamage(20);
        bow.setAttackPerSecond(0.1);
        bow.setSlot(Slot.WEAPON);

        // Act & Assert
        Exception exception = assertThrows(no.accelerate.exceptions.InvalidWeaponException.class,
                () -> warrior1.setEquipment(bow));
        String actual = exception.getMessage();
        assertEquals(expected, actual);

    }
}


