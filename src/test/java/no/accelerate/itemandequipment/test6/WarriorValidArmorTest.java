package no.accelerate.itemandequipment.test5;

import no.accelerate.PrimaryAttribute;
import no.accelerate.heros.Warrior;
import no.accelerate.items.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class WarriorValidArmorTest {

    @Test
    public void warriorSetArmor_validArmorType_shouldReturnTrue() {
        // Arrange
        boolean expected = true;

        Warrior warrior1 = new Warrior();

        Armor mailBody = new Armor();
        mailBody.setName("Mail");
        mailBody.setLevel(1);
        mailBody.setSlot(Slot.BODY);
        mailBody.setArmorType(ArmorType.MAIL);
        mailBody.setAttrArmor(new PrimaryAttribute(1, 0 ,0));
        // Act & Assert
        boolean actual = warrior1.setEquipment(mailBody);
        assertEquals(expected, actual);

    }
}

