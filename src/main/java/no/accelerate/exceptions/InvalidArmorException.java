package no.accelerate.exceptions;

public class InvalidArmorException extends  RuntimeException {
    public InvalidArmorException(String message) {
        super(message);
    }
}
