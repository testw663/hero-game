package no.accelerate.items;

import no.accelerate.PrimaryAttribute;

public class Armor extends Equipment {

    private ArmorType armorType;
    private PrimaryAttribute attrArmor;

    // Constructors

     public Armor(ArmorType armorType, Slot slot, String name) {
        super(slot, name, 1);
        this.armorType = armorType;
    }

    public Armor() {
        super();
    }

    // Getters and setters

    public ArmorType getArmorType() {
        return armorType;
    }

    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }

    public PrimaryAttribute getAttrArmor() {
        return attrArmor;
    }

    public void setAttrArmor(PrimaryAttribute attrArmor) {
        this.attrArmor = attrArmor;
    }

}