package no.accelerate.items;

public enum Slot {
    HEAD,
    BODY,
    LEGS,
    WEAPON
}
