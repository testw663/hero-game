package no.accelerate.items;

public class Equipment extends Item {
    private String name;
    private int level;

    // Constructors

    public Equipment(Slot itemType, String name, int level) {
        super(itemType);
        this.name = name;
        this.level = level;
    }

    public Equipment() {
    }

    // Getters and setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
    }


