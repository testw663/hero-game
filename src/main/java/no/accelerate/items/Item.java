package no.accelerate.items;

public abstract class Item {
    Slot slotType;

    //Constructors

    public Item() {
    }

    public Item(Slot slotTYpe) {
        this.slotType = slotTYpe;
    }

    // Getters and setters

    public void setSlot(Slot slotType) {
        this.slotType = slotType;
    }

    public Slot getSlot() {
        return slotType;
    }

}
