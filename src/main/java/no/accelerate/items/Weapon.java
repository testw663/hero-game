package no.accelerate.items;

public class Weapon extends Equipment{

    WeaponType weaponType;
    int damage;
    double attackPerSecond;

    // Constructors

    public Weapon() {
    }

    public Weapon(WeaponType weaponType, String name, Integer level, Integer damage, Double attackPerSecond) {
        super(Slot.WEAPON,name, level);
        this.weaponType = weaponType;
        this.damage = damage;
        this.attackPerSecond =  attackPerSecond;
    }

    // Getters and setters

    public WeaponType getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(WeaponType weaponType) {
        this.weaponType = weaponType;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public double getAttackPerSecond() {
        return attackPerSecond;
    }

    public void setAttackPerSecond(double attackPerSecond) {

        this.attackPerSecond = attackPerSecond;
    }

    // Methods

    public double getWeaponDPS() {
        return (damage * attackPerSecond);
    }
    }
