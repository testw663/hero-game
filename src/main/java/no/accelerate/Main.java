package no.accelerate;

import no.accelerate.heros.Mage;
import no.accelerate.heros.Warrior;
import no.accelerate.items.*;

public class Main {

    public static void main(String[] args) {

        // Mage - test
        Mage mage1 = new Mage();

/*        Armor plateLegs = new Armor();
        plateLegs.setName("Plate");
        plateLegs.setLevel(1);
        plateLegs.setSlot(Slot.LEGS);
        plateLegs.setArmorType(ArmorType.PLATE);
        plateLegs.setAttrArmor(new PrimaryAttribute(1, 0 ,0));

        mage1.setEquipment(plateLegs);
*/
        Armor clothBody = new Armor();
        clothBody.setName("Cloth");
        clothBody.setLevel(1);
        clothBody.setSlot(Slot.BODY);
        clothBody.setArmorType(ArmorType.CLOTH);
        clothBody.setAttrArmor(new PrimaryAttribute(2, 1 ,0));

        mage1.setEquipment(clothBody);

        Armor clothLeg = new Armor();
        clothLeg.setName("Cloth");
        clothLeg.setLevel(1);
        clothLeg.setSlot(Slot.LEGS);
        clothLeg.setArmorType(ArmorType.CLOTH);
        clothLeg.setAttrArmor(new PrimaryAttribute(0, 4 ,0));

        mage1.setEquipment(clothLeg);

        mage1.levelUp();

        // Warrior - test

        Warrior warrior1 = new Warrior();

        Weapon axe = new Weapon();
        axe.setWeaponType(WeaponType.Axe);
        axe.setName("Axe");
        axe.setLevel(1);
        axe.setDamage(10);
        axe.setAttackPerSecond(0.2);
        axe.setSlot(Slot.WEAPON);

        warrior1.setEquipment(axe);

        System.out.println("Hero name: " + mage1.getName());
        System.out.println("Hero level: " + mage1.getLevel());
        System.out.println("Hero Strength: " + mage1.getStrength());
        System.out.println("Hero Dexterity: " + mage1.getDexterity());
        System.out.println("Hero Intelligence: " + mage1.getIntelligence());
        System.out.println("Total attributes: " + mage1.getTotalAttribute());
        System.out.println("Hero DPS: " + mage1.getDPS());
    }

}
