package no.accelerate;

public class PrimaryAttribute {
    int strength;
    int dexterity;
    int intelligence;

    // Constructors

    public PrimaryAttribute(int strength, int dexterity, int intelligence){
        this.setStrength(strength);
        this.setDexterity(dexterity);
        this.setIntelligence(intelligence);
    }

    // Getters and setters

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }
}
