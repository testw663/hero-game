package no.accelerate.heros;

import no.accelerate.PrimaryAttribute;
import no.accelerate.items.Armor;
import no.accelerate.items.Equipment;
import no.accelerate.items.Slot;
import no.accelerate.items.Weapon;
import java.util.HashMap;
import java.util.Map;

public class Ranger extends Hero {

    PrimaryAttribute attr = new PrimaryAttribute(1, 7, 1);
    int armorAttribute=0;
    int totalAttribute=0;

    // Constructors
    public Ranger() {
        super("Ranger", 1);
    }

    //Methods/Behaviours
    @Override
    public void levelUp() {
        super.levelUp();
        int strength = attr.getStrength();
        int dexterity = attr.getDexterity();
        int intelligence = attr.getIntelligence();
        attr.setStrength(strength+=1);
        attr.setDexterity(dexterity+=5);
        attr.setIntelligence(intelligence+=1);
    }

    public int getArmorAttribute() {
        HashMap<Slot, Equipment> eq = super.getEquipments();
        Armor armor;
        PrimaryAttribute attr2;
        int sumStrength = 0;
        int sumDexterity = 0;
        int sumIntelligence = 0;
        for(Map.Entry<Slot, Equipment> entry : eq.entrySet()) {
            //String key = entry.getKey();
            if (!entry.getKey().name().equals("WEAPON")) {
                //if (entry.getKey().name() "Armor") {
                armor = (Armor) entry.getValue();
                //PrimaryAttribute val2 = value.
                //armor = value.;
                //attr2 = value.;
                attr2 = armor.getAttrArmor();
                sumStrength += attr2.getStrength();
                sumDexterity += attr2.getDexterity();
                sumIntelligence += attr2.getIntelligence();
            }
        }
        armorAttribute = sumStrength + sumDexterity + sumIntelligence;
        return armorAttribute;
    }

    public double getWeaponDPS() {
        HashMap<Slot, Equipment> eq = super.getEquipments();
        Weapon weapon;
        PrimaryAttribute attr2;
        double dps = 0;
        for(Map.Entry<Slot, Equipment> entry : eq.entrySet()) {
            //String key = entry.getKey();
            if (entry.getKey().name().equals("WEAPON")) {
                //if (entry.getKey().name() "Armor") {
                weapon = (Weapon) entry.getValue();
                //PrimaryAttribute val2 = value.
                //armor = value.;
                //attr2 = value.;
                dps = weapon.getWeaponDPS();
            }
        }
        return dps;
    }

    public double getTotalAttribute() {
        //totalAttribute = attr.getStrength() + attr.getDexterity() + attr.getIntelligence() + getArmorAttribute();
        totalAttribute = attr.getDexterity() + getArmorAttribute();
        return totalAttribute;
    }

    public String getName() {
        return super.getName();
    }

    public int getStrength() {
        return attr.getStrength();
    }

    public int getDexterity() {
        return attr.getDexterity();
    }

    public int getIntelligence() {
        return attr.getIntelligence();
    }

    public double getDPS() {
        return getWeaponDPS() * (1 + (getTotalAttribute()/100));
    }

}
