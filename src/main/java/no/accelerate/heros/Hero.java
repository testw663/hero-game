package no.accelerate.heros;

import no.accelerate.PrimaryAttribute;
import no.accelerate.exceptions.InvalidArmorException;
import no.accelerate.exceptions.InvalidWeaponException;
import no.accelerate.items.Equipment;
import no.accelerate.items.Slot;

import java.util.HashMap;

public abstract class Hero {
    // Fields/State
    private String name;
    private int level;
    private final HashMap<Slot, Equipment> equipments = new HashMap<>();

    //Constructors
    public Hero() {
    }
    public Hero(String name, int level) {
        this.name = name;
        this.level = level;
    }

    //Methods/Behaviours
    // Abstract methods
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public void levelUp() {
        this.level += 1;
    }

    public Equipment getEquipment(Slot slot) {
        return equipments.get(slot);
    }

    public boolean setEquipment(Equipment equipment) {
        if (level < equipment.getLevel()) {
            if (Slot.WEAPON.equals(equipment.getSlot())) {
                throw new InvalidWeaponException("The required level for this weapon is too high.");
            } else {
                throw new InvalidArmorException("The required level for this armor is too high.");
            }
        }
        switch (name) {
            case "Mage":
            if (Slot.WEAPON.equals(equipment.getSlot())) {
                if ((!equipment.getName().equals("Staff")) && (!equipment.getName().equals("Wand"))) {
                    throw new InvalidWeaponException("Mage can only use weapons: Staff and Wand");
                }
            }
            else { // Head, Body or Legs
                if (!equipment.getName().equals("Cloth")) {
                    throw new InvalidArmorException("Mage can only use armor: Cloth");
                }
            }
            break;
            case "Rogue":
            if (Slot.WEAPON.equals(equipment.getSlot())) {
                if ((!equipment.getName().equals("Dagger")) && (!equipment.getName().equals("Sword"))) {
                    throw new InvalidWeaponException("Rogue can only use weapons: Dagger and Sword");
                }
            }
            else { // Head, Body or Legs
                if ((!equipment.getName().equals("Leather")) && (!equipment.getName().equals("Mail"))) {
                    throw new InvalidArmorException("Rogue can only use armor: Leather and Mail");
                }
            }
            break;
            case "Ranger":
            if (Slot.WEAPON.equals(equipment.getSlot())) {
                if ((!equipment.getName().equals("Bow"))){
                    throw new InvalidWeaponException("Ranger can only use weapon: Bow");
                }
            }
            else { // Head, Body or Legs
                if ((!equipment.getName().equals("Leather")) && (!equipment.getName().equals("Mail"))) {
                    throw new InvalidArmorException("Ranger can only use armor: Leather and Mail");
                }
            }
            break;
            case "Warrior":
            if (Slot.WEAPON.equals(equipment.getSlot())) {
                if ((!equipment.getName().equals("Axe")) && (!equipment.getName().equals("Hammer") && (!equipment.getName().equals("Sword")))) {
                    throw new InvalidWeaponException("Warrior can only use weapons: Axe, Hammer and Sword");
                }
            }
            else { // Head, Body or Legs
                if ((!equipment.getName().equals("Mail")) && (!equipment.getName().equals("Plate"))) {
                    throw new InvalidArmorException("Warrior can only use armor: Mail and Plate");
                }
            }
            break;
        }
        equipments.put(equipment.getSlot(), equipment);
        return true;
    }

    public HashMap<Slot, Equipment> getEquipments() {
        return equipments;
    }
}
