# Automated testing

This document serves to outline the steps involved in creating a project with
Java and automating the testing process with GitLab-CI.

## Step 1: Create project

Create an empty project from either Gradle or Maven. This project was made with
Gradle.

The project needs to contain the dependency for JUnit, ensure the created
project has this.

Create a `Main.java` class in the proper package relating to your group name.

## Step 2: Add in skeleton for business logic

Include classes to be tested, in this project we are going to test a simple
`PlayableCharacter.java`. This can be found in the `no.accelerate.characters`
package.

## Step 3: Add testing class

For this project, a `PlayableCharacterTest.java` class was added.

Add an empty test to verify everything is working correctly, this test is
named `empty()` and will pass no matter what. It will be removed once the
pipeline is set up.

## Step 4: Adding gitlab-ci.yaml

The core of CI/CD with Gitlab is the `gitlab-ci.yml` file. This contains
instructions for the various jobs and their respective stages.

To read more about Gitlab CI, follow the links below:

- [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/)
- [CI/CD concepts](https://docs.gitlab.com/ee/ci/introduction/)
- [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/)
- [The .gitlab-ci.yml file](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html)
- [Run your CI/CD jobs in Docker containers](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html)
- [Unit test reports](https://docs.gitlab.com/ee/ci/unit_test_reports.html)
- [JUnit report Java](https://docs.gitlab.com/ee/ci/unit_test_reports.html#java-examples)
- [Troubleshooting CI/CD](https://docs.gitlab.com/ee/ci/troubleshooting.html)
- [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/)

> __NOTE__: The relevant information to set up this pipeline will be covered as
> we build it, the links are more context.

We will add `gitlab-ci.yml` to the root of our application alongside `READE.md`
. The contents of the configuration will be from [JUnit report Gradle](https://docs.gitlab.com/ee/ci/unit_test_reports.html#java-examples) and set
to only run when pushed the `master` branch and when `merge_requests` are made.
The
`gradle` image was
added to
prevent it using ruby to run the tests. `clean` was also added to the `gradle
test` command to remove the previous build. This prevents any previous
configuration from tampering with the current build.

The code snippet:

```yml
artifacts:
    when: always
    reports:
      junit: build/test-results/test/**/TEST-*.xml
```

Is present to create a report in Gitlab for how many tests passed and failed.

## Step 5: Code and write tests

Any new code that is added and tested against will run through this pipeline
automatically when pushed to master.

When we have failing tests that try to merge into master, we will not be
allowed to until they pass. The image below shows this process of adding
commits until the branch can be merged.

![Commits to pass](/images/Failing-pipeline.png)
